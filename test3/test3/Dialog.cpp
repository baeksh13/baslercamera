#include "Dialog.h"

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
{
    ui->setupUi(this);

	m_view = ui->graphicsView;

	QPen outlinePen(Qt::blue);
	outlinePen.setWidth(10);

	image = new QImage("test.jpg");

	item = new ImageItem();
	item->setImage(*image);
	scene = new QGraphicsScene(this);
	scene->addItem(item);
	scene->addRect(0,0,(*image).width(), (*image).height(), outlinePen);
	m_view->setScene(scene);
}

//void Dialog::mousePressEvent(QMouseEvent *eve)
//{
//	QPoint position = eve->pos();
//
//	ui->label->setNum(position.x());
//	ui->label_2->setNum(position.y());
//	qDebug("%d/n", position.x());
//	qDebug("%d/n", position.y());
//}
