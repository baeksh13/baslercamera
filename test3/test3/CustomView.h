#pragma once

#include <QGraphicsView>
#include <QtWidgets>
#include <QWidget>

class CustomView :public QGraphicsView
{
public:
	CustomView(QWidget* parent =0 );

protected:
	virtual void wheelEvent(QWheelEvent *event);
};