#pragma once

#include <QtWidgets/QDialog>
#include "ui_Dialog.h"
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QtGui>
#include "ImageItem.h"

class Dialog : public QDialog
{
    Q_OBJECT

public:
    Dialog(QWidget *parent = Q_NULLPTR);
	void mousePressEvent(QMouseEvent *eve);

private:
    Ui::DialogClass *ui;

	QImage *image;
	ImageItem *item;
	QGraphicsScene *scene;
	QGraphicsView *m_view;
};
