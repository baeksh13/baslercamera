#include "ImageItem.h"

#include <QPainter>

ImageItem::ImageItem(void)
	:m_painted(true)
{
}


ImageItem::~ImageItem(void)
{

}

void ImageItem::setImage(QImage& image)
{
	m_image = image;
	update();
}

QRgb ImageItem::getPixel(int x, int y)
{
	return m_image.pixel(QPoint(x, y));
}

//virtual
QRectF ImageItem::boundingRect() const
{
	return QRectF(0, 0, m_image.width(), m_image.height());
}

//virtual
QPainterPath ImageItem::shape() const
{
	QPainterPath path;
	path.addRect(boundingRect());

	return path;
}

void ImageItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	if (m_image.isNull())
		return;

	painter->drawImage(boundingRect(), m_image);
}