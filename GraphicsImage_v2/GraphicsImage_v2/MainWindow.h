#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_MainWindow.h"

#include <pylon/PylonIncludes.h>
#include <QMutex>

using namespace Pylon;

class MainWindow : public QMainWindow
	, public CImageEventHandler
	, public CConfigurationEventHandler
{
    Q_OBJECT

signals:
	void OneImageFinishSignal();
private slots:
	void OneImageFinishSlot();

public:
    MainWindow(QWidget *parent = Q_NULLPTR);
	~MainWindow();

private:
    Ui::MainWindowClass ui;

	CInstantCamera* m_camera;
	// The grab result retrieved from the camera
	CGrabResultPtr m_ptrGrabResult;
	// The grab result as a windows DIB to be displayed on the screen
	CPylonBitmapImage m_bitmapImage;
	CAcquireContinuousConfiguration m_continousConfiguration;

	QMutex m_mutexLock;

	QGraphicsView *m_view;

protected:
	void showImage();
	virtual bool eventFilter(QObject *watched, QEvent *event);

	// Pylon::CImageEventHandler functions
	//virtual void OnImagesSkipped(Pylon::CInstantCamera& camera, size_t countOfSkippedImages);
	virtual void OnImageGrabbed(Pylon::CInstantCamera& camera, const Pylon::CGrabResultPtr& grabResult);

	// Pylon::CConfigurationEventHandler functions
	virtual void OnAttach(Pylon::CInstantCamera& camera);
	virtual void OnAttached(Pylon::CInstantCamera& camera);
	virtual void OnDetach(Pylon::CInstantCamera& camera);
	virtual void OnDetached(Pylon::CInstantCamera& camera);
	virtual void OnDestroy(Pylon::CInstantCamera& camera);
	virtual void OnDestroyed(Pylon::CInstantCamera& camera);
	virtual void OnOpen(Pylon::CInstantCamera& camera);
	virtual void OnOpened(Pylon::CInstantCamera& camera);
	virtual void OnClose(Pylon::CInstantCamera& camera);
	virtual void OnClosed(Pylon::CInstantCamera& camera);
	virtual void OnGrabStart(Pylon::CInstantCamera& camera);
	virtual void OnGrabStarted(Pylon::CInstantCamera& camera);
	virtual void OnGrabStop(Pylon::CInstantCamera& camera);
	virtual void OnGrabStopped(Pylon::CInstantCamera& camera);
	virtual void OnGrabError(Pylon::CInstantCamera& camera, const char* errorMessage);
	virtual void OnCameraDeviceRemoved(Pylon::CInstantCamera& camera);
};
