#include "MainWindow.h"

#include <qgraphicsitem.h>
#include "ImageItem.h"
#include <qgraphicsview.h>
#include <QDebug>
#include <pylon/PylonGUI.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);

	PylonInitialize();

	try
	{
		m_camera = new CInstantCamera((CTlFactory::GetInstance().CreateFirstDevice()));
		m_camera->RegisterImageEventHandler(this, Pylon::RegistrationMode_ReplaceAll, Pylon::Cleanup_Delete);
		// Register this object as a configuration event handler, so we will be notified of camera state changes.
		// See Pylon::CConfigurationEventHandler for details
		m_camera->RegisterConfiguration(this, Pylon::RegistrationMode_ReplaceAll, Pylon::Cleanup_Delete);

		//m_camera.Attach(Pylon::CTlFactory::GetInstance().CreateFirstDevice(), Pylon::Cleanup_Delete);

		m_camera->Open();

		// Camera may have been disconnected.
		/*if (!m_camera.IsOpen() || m_camera.IsGrabbing())
		{
			return;
		}*/

		//m_continousConfiguration.OnOpened(m_camera);

		m_camera->StartGrabbing(GrabStrategy_OneByOne, GrabLoop_ProvidedByInstantCamera);


	}
	catch (const GenericException& e)
	{

	}

	ui.centralWidget->installEventFilter(this);

	connect(this, SIGNAL(OneImageFinishSignal()), this, SLOT(OneImageFinishSlot()));
}

MainWindow::~MainWindow()
{
	if (m_camera)
	{
		m_camera->Close();
		delete m_camera;
		m_camera = NULL;
	}
	PylonTerminate();
}

void MainWindow::OnImageGrabbed(Pylon::CInstantCamera& camera, const Pylon::CGrabResultPtr& grabResult)
{

	m_mutexLock.lock();

	m_ptrGrabResult = grabResult;

	emit OneImageFinishSignal();

	m_mutexLock.unlock();

}

void MainWindow::OneImageFinishSlot()
{
	//qDebug() << __FUNCTION__;
	ui.centralWidget->update();
}

bool MainWindow::eventFilter(QObject *watched, QEvent *event)
{
	if (watched == ui.centralWidget && event->type() == QEvent::Paint)
	{
		showImage();
	}
	return false;
}
void MainWindow::showImage()
{

	m_mutexLock.lock();

	CImageFormatConverter formatConverter;
	formatConverter.OutputPixelFormat = PixelType_Mono8;
	formatConverter.Convert(m_bitmapImage, m_ptrGrabResult);
	QImage qimage = QImage((uchar*)m_bitmapImage.GetBuffer(), m_bitmapImage.GetWidth(), m_bitmapImage.GetHeight(), QImage::Format_Grayscale8);

	m_view = ui.graphicsView;

	QBrush greenBrush(Qt::green);
	QPen outlinePen(Qt::black);
	outlinePen.setWidth(2);

	ImageItem* item = new ImageItem();
	item->setImage(qimage);
	/*QGraphicsScene* scene = new QGraphicsScene(ui->graphicsView);*/
	QGraphicsScene* scene = new QGraphicsScene(this);
	scene->addItem(item);
	scene->addRect(0, 100, 100, 100, outlinePen, greenBrush);
	m_view->setScene(scene);

	m_mutexLock.unlock();
}

// Pylon::CConfigurationEventHandler functions
void MainWindow::OnAttach(Pylon::CInstantCamera& camera)
{
	qDebug() << __FUNCTION__;
}


void MainWindow::OnAttached(Pylon::CInstantCamera& camera)
{
	qDebug() << __FUNCTION__;
}


void MainWindow::OnDetach(Pylon::CInstantCamera& camera)
{
	qDebug() << __FUNCTION__;
}


void MainWindow::OnDetached(Pylon::CInstantCamera& camera)
{
	qDebug() << __FUNCTION__;
}


void MainWindow::OnDestroy(Pylon::CInstantCamera& camera)
{
	qDebug() << __FUNCTION__;
}


void MainWindow::OnDestroyed(Pylon::CInstantCamera& camera)
{
	qDebug() << __FUNCTION__;
}


void MainWindow::OnOpen(Pylon::CInstantCamera& camera)
{
	Pylon::String_t strFriendlyName = camera.GetDeviceInfo().GetFriendlyName();
	qDebug() << __FUNCTION__ << " - " << strFriendlyName.c_str();
}


void MainWindow::OnOpened(Pylon::CInstantCamera& camera)
{
	qDebug() << __FUNCTION__;
}


void MainWindow::OnClose(Pylon::CInstantCamera& camera)
{
	qDebug() << __FUNCTION__;
}


void MainWindow::OnClosed(Pylon::CInstantCamera& camera)
{
	Pylon::String_t strFriendlyName = camera.GetDeviceInfo().GetFriendlyName();
	qDebug() << __FUNCTION__ << " - " << strFriendlyName.c_str();
}


void MainWindow::OnGrabStart(Pylon::CInstantCamera& camera)
{
	qDebug() << __FUNCTION__;
}


void MainWindow::OnGrabStarted(Pylon::CInstantCamera& camera)
{
	qDebug() << __FUNCTION__;
}


void MainWindow::OnGrabStop(Pylon::CInstantCamera& camera)
{
	qDebug() << __FUNCTION__;
}


void MainWindow::OnGrabStopped(Pylon::CInstantCamera& camera)
{
	qDebug() << __FUNCTION__;
	//m_camera.DeregisterConfiguration(&m_continousConfiguration);
}


void MainWindow::OnGrabError(Pylon::CInstantCamera& camera, const char* errorMessage)
{
	qDebug() << __FUNCTION__;
}


void MainWindow::OnCameraDeviceRemoved(Pylon::CInstantCamera& camera)
{
	qDebug() << __FUNCTION__;
}

