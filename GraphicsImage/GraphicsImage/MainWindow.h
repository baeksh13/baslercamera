#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_MainWindow.h"
#include <qgraphicsview.h>
#include <pylon/GrabResultPtr.h>
#include <pylon/stdinclude.h>

class CSampleImageEventHandler;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = Q_NULLPTR);

	//void Init();

	//void ContinuousShot();
	//void ShowImage(QImage image);

	//QImage *DisplayImage();

	void Camera();

public slots:
	void ShowImage();

private:
    Ui::MainWindowClass *ui;
	QGraphicsView *m_view;
	CSampleImageEventHandler* handle;

//public slots:
//	void setValue(int val);


};


//class SignalSlot : public QWidget
//{
//	Q_OBJECT
//
//public:
//	void setValue(int val) {
//		emit valueChanged(val);
//	}
//
//signals:
//	void valueChanged(int newValue);
//
//private:
//	int m_value;
//};

