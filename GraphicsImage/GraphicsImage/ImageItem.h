#pragma once

#include <QGraphicsItem>

class ImageItem : public QGraphicsItem
{
public:
	ImageItem(void);
	~ImageItem(void);

	virtual QRectF boundingRect() const;
	virtual QPainterPath shape() const;
	virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget);
	void setImage(QImage& image);
	QRgb getPixel(int x, int y);
	QImage getImage() { return m_image; }

protected:
	QImage m_image;
	volatile bool m_painted;
};
