﻿#include "MainWindow.h"
#include "ui_mainwindow.h"

#include <qfiledialog.h>
#include <qgraphicsitem.h>
#include <qgraphicsview.h>
#include "ImageItem.h"

#include <pylon/PylonIncludes.h>
#include <pylon/PylonGUI.h>
#include "ConfigurationEventPrinter.h"
#include "ImageEventPrinter.h"

#include <QDebug>


// Namespace for using pylon objects.
using namespace Pylon;

// Namespace for using cout.
using namespace std;

// Number of images to be grabbed.
//static const uint32_t c_countOfImagesToGrab = 10;

//Example of an image event handler.
//class CSampleImageEventHandler : public CImageEventHandler, QObject
//{
//	Q_OBJECT
//
//public:
//	void setValue(QImage qImage) {
//		emit SendImage(qImage);
//	}
//
//signals:
//	void SendImage(QImage qImage);
//
//public:
//	virtual void OnImageGrabbed(CInstantCamera& /*camera*/, const CGrabResultPtr& ptrGrabResult)
//	{
//		cout << "CSampleImageEventHandler::OnImageGrabbed called." << std::endl;
//
//		// CGrabResultPtr에서 이미지 형태 및 QImage로 변환
//		CPylonImage target;
//		CImageFormatConverter converter;
//		converter.OutputPixelFormat = PixelType_Mono8;
//		converter.OutputBitAlignment = OutputBitAlignment_MsbAligned;
//		converter.Convert(target, ptrGrabResult);
//		QImage qimage = QImage((uchar*)target.GetBuffer(), target.GetWidth(), target.GetHeight(), QImage::Format_Grayscale8);
//
//		emit SendImage(qimage);
//
//	}
//};

QImage test;

class CSampleImageEventHandler : public CImageEventHandler, QObject
{
	Q_OBJECT

//public:
//	void setValue(QImage qImage) {
//		emit ImageShow(qImage);
//	}

signals:
	void ImageShow();

public:
	virtual void OnImageGrabbed(CInstantCamera& /*camera*/, const CGrabResultPtr& ptrGrabResult)
	{
		cout << "CSampleImageEventHandler::OnImageGrabbed called." << std::endl;

		// CGrabResultPtr에서 이미지 형태 및 QImage로 변환
		CPylonImage target;
		CImageFormatConverter converter;
		converter.OutputPixelFormat = PixelType_Mono8;
		converter.OutputBitAlignment = OutputBitAlignment_MsbAligned;
		converter.Convert(target, ptrGrabResult);
		QImage test = QImage((uchar*)target.GetBuffer(), target.GetWidth(), target.GetHeight(), QImage::Format_Grayscale8);

		emit ImageShow();

	}
};

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent), ui(new Ui::MainWindowClass)
{
	ui->setupUi(this);

	Camera();

	m_view = ui->graphicsView;

	connect(this, SIGNAL(ImageShow()), this, SLOT(ShowImage()));

	//SignalSlot myObject;
	//connect(&myObject, &SignalSlot::valueChanged,this, &MainWindow::setValue);

	//myObject.setValue(50);

	//Init();

	//ContinuousShot();

}

void MainWindow::Camera()
{
	// Before using any pylon methods, the pylon runtime must be initialized.
	PylonInitialize();

	try
	{
		// Create an instant camera object for the camera device found first.
		CInstantCamera camera(CTlFactory::GetInstance().CreateFirstDevice());

		// Register the standard configuration event handler for enabling software triggering.
		// The software trigger configuration handler replaces the default configuration
		// as all currently registered configuration handlers are removed by setting the registration mode to RegistrationMode_ReplaceAll.
		camera.RegisterConfiguration(new CSoftwareTriggerConfiguration, RegistrationMode_ReplaceAll, Cleanup_Delete);

		// For demonstration purposes only, add a sample configuration event handler to print out information about camera use.
		camera.RegisterConfiguration(new CConfigurationEventPrinter, RegistrationMode_Append, Cleanup_Delete);

		// The image event printer serves as sample image processing.
		// When using the grab loop thread provided by the Instant Camera object, an image event handler processing the grab
		// results must be created and registered.
		handle = new CSampleImageEventHandler();
		camera.RegisterImageEventHandler(handle, RegistrationMode_Append, Cleanup_Delete);

		// Open the camera device.
		camera.Open();

		// Can the camera device be queried whether it is ready to accept the next frame trigger?
		if (camera.CanWaitForFrameTriggerReady())
		{
			// Start the grabbing using the grab loop thread, by setting the grabLoopType parameter to GrabLoop_ProvidedByInstantCamera. 
			// The grab results are delivered to the image event handlers.
			// The GrabStrategy_OneByOne default grab strategy is used.
			camera.StartGrabbing(GrabStrategy_OneByOne, GrabLoop_ProvidedByInstantCamera);

			// Wait for user input to trigger the camera or exit the program.
			// The grabbing is stopped, the device is closed and destroyed automatically when the camera object goes out of scope.

			bool runLoop = true;
			while (runLoop)
			{

				// Execute the software trigger. Wait up to 1000 ms for the camera to be ready for trigger.
				if (camera.WaitForFrameTriggerReady(1000, TimeoutHandling_ThrowException))
				{
					camera.ExecuteSoftwareTrigger();
				}

				// Wait some time to allow the OnImageGrabbed handler print its output,
				// so the printed text on the console is in the expected order.
				WaitObject::Sleep(250);

			}
		}
		else
		{
			// See the documentation of CInstantCamera::CanWaitForFrameTriggerReady() for more information.
			cout << endl << "This sample can only be used with cameras that can be queried whether they are ready to accept the next frame trigger." << endl;
		}
	}
	catch (const GenericException& e)
	{
		cerr << "An exception occurred." << endl << e.GetDescription() << endl;
	}
}

void MainWindow::ShowImage()
{
	m_view = ui->graphicsView;

	QBrush greenBrush(Qt::green);
	QPen outlinePen(Qt::black);
	outlinePen.setWidth(2);

	ImageItem* item = new ImageItem();
	item->setImage(test);
	QGraphicsScene* scene = new QGraphicsScene(this);
	scene->addItem(item);
	scene->addRect(0, 100, 100, 100, outlinePen, greenBrush);
	m_view->setScene(scene);
}

//void MainWindow::setValue(int val)
//{
//	QString labelText = QString("signal generate, Value : %1").arg(val);
//	ui->label->setText(labelText);
//}


//void MainWindow::Init()
//{
//	m_view = ui->graphicsView;
//
//	QImage* image = new QImage("test.png");
//	QBrush greenBrush(Qt::green);
//	QPen outlinePen(Qt::black);
//	outlinePen.setWidth(2);
//
//	ImageItem* item = new ImageItem();
//	item->setImage(*image);
//	QGraphicsScene* scene = new QGraphicsScene(this);
//	scene->addItem(item);
//	scene->addRect(0, 100, 100, 100, outlinePen, greenBrush);
//	m_view->setScene(scene);
//
//}

//void MainWindow::ShowImage(QImage image)
//{
//	m_view = ui->graphicsView;
//
//	QImage* dispalyImage = new QImage(image);
//	QBrush greenBrush(Qt::green);
//	QPen outlinePen(Qt::black);
//	outlinePen.setWidth(2);
//
//	ImageItem* item = new ImageItem();
//	item->setImage(*dispalyImage);
//	/*QGraphicsScene* scene = new QGraphicsScene(ui->graphicsView);*/
//	QGraphicsScene* scene = new QGraphicsScene();
//	scene->addItem(item);
//	scene->addRect(0, 100, 100, 100, outlinePen, greenBrush);
//	m_view->setScene(scene);
//};

//Example of an image event handler.
//class CSampleImageEventHandler : public CImageEventHandler
//{
//
//public:
//	virtual void OnImageGrabbed(CInstantCamera& /*camera*/, const CGrabResultPtr& ptrGrabResult)
//	{
//		cout << "CSampleImageEventHandler::OnImageGrabbed called." << std::endl;
//
//		// software trigger 로 연속으로 window에 보여짐
//		//Pylon::DisplayImage(1, ptrGrabResult);
//
//		// CGrabResultPtr에서 이미지 형태 및 QImage로 변환
//		CPylonImage target;
//		CImageFormatConverter converter;
//		converter.OutputPixelFormat = PixelType_Mono8;
//		converter.OutputBitAlignment = OutputBitAlignment_MsbAligned;
//		converter.Convert(target, ptrGrabResult);
//		QImage qimage = QImage((uchar*)target.GetBuffer(), target.GetWidth(), target.GetHeight(), QImage::Format_Grayscale8);
//
//		MainWindow *window;
//		window->ShowImage(qimage);
//	}
//};

//void MainWindow::ContinuousShot()
//{
//	// Before using any pylon methods, the pylon runtime must be initialized.
//	PylonInitialize();
//
//	try
//	{
//		// Create an instant camera object for the camera device found first.
//		CInstantCamera camera(CTlFactory::GetInstance().CreateFirstDevice());
//
//		// Register the standard configuration event handler for enabling software triggering.
//		// The software trigger configuration handler replaces the default configuration
//		// as all currently registered configuration handlers are removed by setting the registration mode to RegistrationMode_ReplaceAll.
//		camera.RegisterConfiguration(new CSoftwareTriggerConfiguration, RegistrationMode_ReplaceAll, Cleanup_Delete);
//
//		// For demonstration purposes only, add a sample configuration event handler to print out information about camera use.
//		camera.RegisterConfiguration(new CConfigurationEventPrinter, RegistrationMode_Append, Cleanup_Delete);
//
//		// For demonstration purposes only, register another image event handler.
//		camera.RegisterImageEventHandler(new CSampleImageEventHandler, RegistrationMode_Append, Cleanup_Delete);
//
//		// Open the camera device.
//		camera.Open();
//
//		// Can the camera device be queried whether it is ready to accept the next frame trigger?
//		if (camera.CanWaitForFrameTriggerReady())
//		{
//			// Start the grabbing using the grab loop thread, by setting the grabLoopType parameter to GrabLoop_ProvidedByInstantCamera. 
//			// The grab results are delivered to the image event handlers.
//			// The GrabStrategy_OneByOne default grab strategy is used.
//			camera.StartGrabbing(GrabStrategy_OneByOne, GrabLoop_ProvidedByInstantCamera);
//
//			// Wait for user input to trigger the camera or exit the program.
//			// The grabbing is stopped, the device is closed and destroyed automatically when the camera object goes out of scope.
//
//			bool runLoop = true;
//			while (runLoop)
//			{
//
//				// Execute the software trigger. Wait up to 1000 ms for the camera to be ready for trigger.
//				if (camera.WaitForFrameTriggerReady(1000, TimeoutHandling_ThrowException))
//				{
//					camera.ExecuteSoftwareTrigger();
//				}
//
//				// Wait some time to allow the OnImageGrabbed handler print its output,
//				// so the printed text on the console is in the expected order.
//				WaitObject::Sleep(250);
//
//			}
//		}
//		else
//		{
//			// See the documentation of CInstantCamera::CanWaitForFrameTriggerReady() for more information.
//			cout << endl << "This sample can only be used with cameras that can be queried whether they are ready to accept the next frame trigger." << endl;
//		}
//
//	}
//	catch (const GenericException& e)
//	{
//		// Error handling.
//		cerr << "An exception occurred." << endl << e.GetDescription() << endl;
//	}
//}

//QImage* MainWindow::DisplayImage()
//{
//	 Before using any pylon methods, the pylon runtime must be initialized.
//	PylonInitialize();
//
//	try
//    {
//         Create an instant camera object with the camera device found first.
//        CInstantCamera camera( CTlFactory::GetInstance().CreateFirstDevice() );
//
//         Print the model name of the camera.
//        cout << "Using device " << camera.GetDeviceInfo().GetModelName() << endl;
//
//         The parameter MaxNumBuffer can be used to control the count of buffers
//         allocated for grabbing. The default value of this parameter is 10.
//        camera.MaxNumBuffer = 3;
//
//         Start the grabbing of c_countOfImagesToGrab images.
//         The camera device is parameterized with a default configuration which sets up free-running continuous acquisition.
//        camera.StartGrabbing( c_countOfImagesToGrab );
//
//         This smart pointer will receive the grab result data.
//        CGrabResultPtr ptrGrabResult;
//		int count = 0;
//
//         Camera.StopGrabbing() is called automatically by the RetrieveResult() method
//         when c_countOfImagesToGrab images have been retrieved.
//        while (camera.IsGrabbing())
//        {
//             Wait for an image and then retrieve it. A timeout of 5000 ms is used.
//            camera.RetrieveResult( 5000, ptrGrabResult, TimeoutHandling_ThrowException );
//
//             Image grabbed successfully?
//            if (ptrGrabResult->GrabSucceeded())
//            {
//				CPylonImage target;
//				 Convert to correct format for vision system
//				CImageFormatConverter converter;
//				converter.OutputPixelFormat = PixelType_Mono8;
//				converter.OutputBitAlignment = OutputBitAlignment_MsbAligned;
//				converter.Convert(target, ptrGrabResult);
//
//				QImage* qimage = new QImage((uchar*)target.GetBuffer(), target.GetWidth(), target.GetHeight(), QImage::Format_Grayscale8);
//
//				return qimage;
//            }
//            else
//            {
//                cout << "Error: " << std::hex << ptrGrabResult->GetErrorCode() << std::dec << " " << ptrGrabResult->GetErrorDescription() << endl;
//            }
//        }
//    }
//    catch (const GenericException& e)
//    {
//         Error handling.
//        cerr << "An exception occurred." << endl
//            << e.GetDescription() << endl;
//    }
//}

