#pragma once
#include <QMainWindow>
#include <QGraphicsScene>

class ImageView : public QMainWindow
{
	Q_OBJECT

public:
	explicit ImageView(QWidget *parent = Q_NULLPTR);

private:
	void setCamera();
	QGraphicsScene *m_scene;

};