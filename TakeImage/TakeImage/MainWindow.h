#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_MainWindow.h"
#include "ImageView.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = Q_NULLPTR);

private slots:
	void takePicture();
};
