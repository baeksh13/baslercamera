#include "MainWindow.h"
#include <QMenu>
#include <QAction>
#include <QMenuBar>
#include <QToolBar>
#include <QDockWidget>
#include <QListWidget>
#include <QStatusBar>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
	QMenu *fileMenu;
	QAction *takePic;

	QWidget *widget = new QWidget();
	widget->resize(1080, 720);
	takePic = new QAction(QIcon("C:/Users/nt090/source/repos/baslercamera/TakeImage/TakeImage/camera.png"), tr("TP"), this);
	connect(takePic, SIGNAL(triggered()), this, SLOT(takePicture()));

	fileMenu = menuBar()->addMenu(tr("&File"));
	fileMenu->addAction(takePic);

	QToolBar *fileToolBar;
	fileToolBar = addToolBar(tr("File"));
	fileToolBar->addAction(takePic);
}

void MainWindow::takePicture()
{
	//qDebug() << Q_FUNC_INFO;
	setCentralWidget(new ImageView());

}