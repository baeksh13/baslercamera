#include "ImageView.h"
#include <QMdiArea>
#include <QMdiSubWindow>
#include <QLabel>
#include <QVBoxLayout>
#include <QDebug>

//load pylon api
#include <pylon/PylonIncludes.h>
#include <pylon/PylonGUI.h>

//namespace 
using namespace Pylon;
using namespace std;
using namespace GenApi;

// Number of images to be grabbed.
static const uint32_t c_countOfImagesToGrab = 1;

ImageView::ImageView(QWidget *parent)
	: QMainWindow(parent)
{
	//QMdiArea* area = new QMdiArea();
	//area->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

	//// MdiSubWindow 생성
	//QMdiSubWindow* subWindow1 = new QMdiSubWindow();
	//subWindow1->resize(480, 360);

	//// MDIMainWindows에 서브 윈도우 추가
	//area->addSubWindow(subWindow1);

	/*QImage image;
	image = QImage("C:/Users/nt090/source/repos/baslertest/test20220209/test20220209/img_20220209190141.bmp");*/

	setCamera();

	/*QLabel *label_2 = new QLabel(this);
	label_2->setPixmap(QPixmap::fromImage(image));*/

	/*QScrollArea *area_2;
	area_2 = new QScrollArea(this);
	area_2->setWidget(label_2);
	area_2->setBackgroundRole(QPalette::Dark);*/

	/*setCentralWidget(area);*/
	/*setCentralWidget(area_2);*/

	this->setWindowFlags(Qt::Widget);
}

void ImageView::setCamera()
{

	QMdiArea* area = new QMdiArea();
	area->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

	// MdiSubWindow 생성
	QMdiSubWindow* subWindow1 = new QMdiSubWindow();
	subWindow1->resize(480, 360);

	// MDIMainWindows에 서브 윈도우 추가
	area->addSubWindow(subWindow1);

	// Before using any pylon methods, the pylon runtime must be initialized.
	PylonInitialize();

	char buf[256];

	try
	{
		QWidget *widget = new QWidget;

		QLabel *label = new QLabel(this);
		QLabel *label_2 = new QLabel(this);

		QVBoxLayout *layout = new QVBoxLayout(widget);
		layout->addWidget(label);

		// Create an instant camera object for the camera device found first.
		CInstantCamera camera(CTlFactory::GetInstance().CreateFirstDevice());

		QString cameraName = camera.GetDeviceInfo().GetModelName();
		label->setText(cameraName);

		// The parameter MaxNumBuffer can be used to control the count of buffers
		// allocated for grabbing. The default value of this parameter is 10.
		camera.MaxNumBuffer = 5;

		// Start the grabbing of c_countOfImagesToGrab images.
		// The camera device is parameterized with a default configuration which
		// sets up free-running continuous acquisition.
		camera.StartGrabbing(c_countOfImagesToGrab);

		// This smart pointer will receive the grab result data.
		CGrabResultPtr ptrGrabResult;

		// Camera.StopGrabbing() is called automatically by the RetrieveResult() method
		// when c_countOfImagesToGrab images have been retrieved.
		while (camera.IsGrabbing())
		{
			// Wait for an image and then retrieve it. A timeout of 5000 ms is used.
			camera.RetrieveResult(5000, ptrGrabResult, TimeoutHandling_ThrowException);

			// Image grabbed successfully?
			if (ptrGrabResult->GrabSucceeded())
			{
				CPylonImage target;
				// Convert to correct format for vision system
				CImageFormatConverter converter;
				converter.OutputPixelFormat = PixelType_Mono8;
				converter.OutputBitAlignment = OutputBitAlignment_MsbAligned;
				converter.Convert(target, ptrGrabResult);

				qDebug() << target.GetWidth();
				qDebug() << target.GetHeight();
				qDebug() << target.GetPixelType();

				//time_t timer;
				//struct tm today;
				//timer = time(NULL); // 현재 시각을 초 단위
				//localtime_s(&today, &timer); // 초 단위의 시간을 분리

				//sprintf(buf, "C:/Users/nt090/source/repos/baslertest/test20220209/test20220209/img_%04d%02d%02d%02d%02d%02d.bmp",
				//	today.tm_year + 1900,
				//	today.tm_mon + 1,
				//	today.tm_mday,
				//	today.tm_hour,
				//	today.tm_min,
				//	today.tm_sec);
				//target.Save(ImageFileFormat_Bmp, buf);

				QImage qimage;
				// grayscale image
				qimage = QImage((uchar*)target.GetBuffer(), target.GetWidth(), target.GetHeight(), QImage::Format_Grayscale8);

				QImage scaledImage = qimage.scaled(label_2->size(), Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);
				label_2->setPixmap(QPixmap::fromImage(scaledImage));
				layout->addWidget(label_2);

				subWindow1->setWidget(widget);

				setCentralWidget(area);

			}
			else
			{
				cout << "Error: " << std::hex << ptrGrabResult->GetErrorCode() << std::dec << " " << ptrGrabResult->GetErrorDescription() << endl;
			}
		}

	}
	catch (const GenericException& e)
	{
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
	}
}
